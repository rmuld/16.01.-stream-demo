package ee.bcs.valiit;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {

    public static boolean Paaris(int arv) {return arv % 2 == 0;}

    public static void main(String[] args) {
	// write your code here

        //Striimid
        var arvud = IntStream.range(0,10).toArray();  //arvud on int[]
        System.out.println(IntStream.range(0,10).sum());
        var imelikudArvud = IntStream.range(0,20)
                .filter(x -> x > 10);

        Random r = new Random();
        IntStream
                .iterate(0, x -> r.nextInt(52))
                .distinct()
                .limit(5)
                //.forEach(System.out::println)
                ;
        //for(var kaart : pakk) System.out.printf("arv on %s\t", kaart);

        //for (var x : imelikudArvud.filter(Main::Paaris).toArray())
                //System.out.println(x);

        //lambda avaldis (x -> x % 2 == 0)
        //String[] nimed = {"Riina", "Rita", "Reet"};
        //IntStream.of(nimed).filter(x -> x > 10).collector(Collectors.toList());
        //System.out.println(nimed);


        //Stream.of(nimed)
        //.filter(x -> x.length() == 4).forEach(System.out::println);


        IntStream.range(0,20)
                .map(x -> x*x)
                .boxed()
                .map(x -> String.format("arv on %s \t", x))
                //.forEach(System.out::println)
                .count()
                //.collect(Collectors.toList())
                ;
    }
}
